import os
import sys
import json
sys.path.append('/var/www/freetime-finder')

activate_this = '/var/www/freetime-finder/venv/bin/activate_this.py'
#execfile(activate_this, dict(__file__=activate_this))
exec(open(activate_this).read(), dict(__file__=activate_this))
from app import create_app

application = create_app()

