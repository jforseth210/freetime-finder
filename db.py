from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()

user_event_invitation_association = db.Table(
    'user_event_invitation_association',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.Column('event_id', db.Integer, db.ForeignKey('event.id'), primary_key=True)
)
user_event_acceptance_association = db.Table(
    'user_event_acceptance_association',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.Column('event_id', db.Integer, db.ForeignKey('event.id'), primary_key=True)
)
user_event_declination_association = db.Table(
    'user_event_declination_association',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.Column('event_id', db.Integer, db.ForeignKey('event.id'), primary_key=True)
)