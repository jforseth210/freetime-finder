from db import db, user_event_acceptance_association, user_event_invitation_association, user_event_declination_association

from sqlalchemy import Column, Integer, String, Text, ForeignKey, DateTime
from sqlalchemy.orm import relationship
from datetime import datetime

class Event(db.Model):
    __tablename__ = "event"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    hours = db.Column(db.Integer, nullable=False)
    minutes = db.Column(db.Integer, nullable=False)
    first_date = db.Column(db.DateTime, nullable=False)
    last_date = db.Column(db.DateTime, nullable=False)
    
    start_after = db.Column(db.Time, nullable=False, default=datetime.min.time())
    end_before = db.Column(db.Time, nullable=False, default=datetime.max.time())

    # Define many-to-many relationship with User
    invited_users = db.relationship('User', secondary=user_event_invitation_association, backref='invited_events', lazy='dynamic')
    accepted_users = db.relationship('User', secondary=user_event_acceptance_association, backref='accepted_events', lazy='dynamic')
    declined_users = db.relationship('User', secondary=user_event_declination_association, backref='declined_events', lazy='dynamic')
    
    # Add the creator field
    creator_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    creator = db.relationship('User', backref='created_events')
    
    # Define a one-to-many relationship with TimeSlot
    time_slots = relationship('TimeSlot', back_populates='event', cascade='all, delete-orphan')
    
    def __init__(self, creator, name, hours, minutes, first_date, last_date, start_after, end_before, invitees):
        self.creator = creator
        self.name = name
        self.hours = hours
        self.minutes = minutes
        self.first_date = first_date
        self.last_date = last_date
        self.start_after = start_after
        self.end_before = end_before
        self.invited_users = invitees
class TimeSlot(db.Model):
    __tablename__ = 'time_slot'
    id = Column(Integer, primary_key=True)
    event_id = Column(Integer, ForeignKey('event.id'))
    start_time = Column(DateTime)
    end_time = Column(DateTime)

    # Define a many-to-one relationship with Event
    event = relationship('Event', back_populates='time_slots')
