from db import db
class CaldavCredential(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(100), nullable=False)
    username = db.Column(db.String(50), nullable=False)
    password = db.Column(db.String(50), nullable=False)
    
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    def __init__(self, user, url,username, password):
        self.user = user
        self.url = url
        self.username = username
        self.password = password
class Webcal(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(200), nullable=False)
    name = db.Column(db.String(255))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    #user = db.relationship(
    #    "user", backref=db.backref("webcals", lazy=True))
    def __init__(self, user, url):
        self.user = user
        self.url = url
class CalFile(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    contents = db.Column(db.Text, nullable=False)
    
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    def __init__(self, user, contents):
        self.user = user
        self.contents = contents

    def get_name(self):
        names = [line.replace("X-WR-CALNAME:","\n") for line in self.contents.decode("utf-8").split("\n") if "X-WR-CALNAME:" in line]
        if len(names) == 0:
            return "Unnamed Calendar"
        return names[0]

