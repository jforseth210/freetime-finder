from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin
from datetime import datetime
from db import db, user_event_acceptance_association, user_event_invitation_association
from werkzeug.security import generate_password_hash, check_password_hash
from models.Calendar import Webcal, CalFile, CaldavCredential

class User(UserMixin, db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    hashed_password = db.Column(db.String(255), nullable=False)

    # Define a one-to-many relationship with CaldavCredentials
    caldav_credentials = db.relationship('CaldavCredential', backref='user', lazy=True)
    
    # Define a one-to-many relationship with Webcals
    webcals = db.relationship('Webcal', backref='user', lazy=True)
    
    # Define a one-to-many relationship with CalFiles
    calfiles = db.relationship('CalFile', backref='user', lazy=True)

    # Define many-to-many relationship with Event
    #invited_events = db.relationship('Event', secondary=user_event_invitation_association, backref='invited_users', lazy='dynamic')
    #accepted_events = db.relationship('Event', secondary=user_event_acceptance_association, backref='accepted_users', lazy='dynamic')
    
    def __init__(self, username, password):
        self.username = username
        self.hashed_password = generate_password_hash(password)

