import caldav
from caldav.elements import dav
from ics import Calendar
import requests
from dateutil import tz
from datetime import datetime, timedelta, date, time
import pytz
from dateutil.rrule import rrulestr
import arrow
from tzlocal import get_localzone
from models.User import User
import logging
logger = logging.getLogger(__name__)


def get_acceptee_events(event):
    logger.info('Starting to get accepted events for the event')
    
    users = event.accepted_users
    first_date = arrow.get(event.first_date)
    last_date = arrow.get(event.last_date)
    all_events = []
    
    logger.debug(f'Event time range: {first_date} to {last_date}')
    
    for user in users:
        logger.debug(f'Processing user: {user}')
        
        for caldav in user.caldav_credentials:
            logger.debug(f'Fetching CalDAV events for user: {user}, URL: {caldav.url}')
            try:
                events = get_caldav_events(caldav.url, caldav.username, caldav.password, first_date, last_date)
                all_events.extend(events)
                logger.debug(f'Fetched {len(events)} events from CalDAV for user: {user}')
            except Exception as e:
                logger.error(f'Error fetching CalDAV events for user: {user}, URL: {caldav.url}: {e}')
        
        for webcal in user.webcals:
            url = webcal.url.replace("webcal://", "https://")
            logger.info(f'Fetching Webcal events for user: {user}, URL: {url}')
            try:
                response = requests.get(url)
                events = get_ics_events(response.text, first_date, last_date)
                all_events.extend(events)
                logger.info(f'Fetched {len(events)} events from Webcal for user: {user}')
            except Exception as e:
                logger.error(f'Error fetching Webcal events for user: {user}, URL: {url}: {e}')
        
        for calfile in user.calfiles:
            logger.info(f'Fetching Calfile events for user: {user}')
            try:
                events = get_ics_events(calfile.contents, first_date, last_date)
                all_events.extend(events)
                logger.info(f'Fetched {len(events)} events from Calfile for user: {user}')
            except Exception as e:
                logger.error(f'Error fetching Calfile events for user: {user}: {e}')
    
    logger.info('Finished fetching accepted events for the event')
    return all_events


def merge_overlapping_events(events):
    if not events:
        logger.warning('No events to merge')
        return []

    # Sort the ranges by start time
    sorted_events = sorted(events, key=lambda x: x[0])

    # Initialize the merged ranges list with the first range
    merged_events = [sorted_events[0]]

    for current_event in sorted_events[1:]:
        last_event = merged_events[-1]

        # If the current range overlaps with the last range in the merged list,
        # update the end time of the last range to the later of the two end times
        if current_event[0] <= last_event[1]:
            merged_events[-1] = (last_event[0], max(last_event[1], current_event[1]))

        # If the current range does not overlap, add it to the list
        else:
            merged_events.append(current_event)

    return merged_events
def find_free_times(events, start_after, end_before):
    logger.info('Finding free times between %s and %s', start_after, end_before)
    events = merge_overlapping_events(events)
    # If there are no events, there are no gaps to find
    if not events:
        return []

    # Sort the events
    events.sort(key=lambda x: x[1])

    free_times = []
    previous_event_end_time = events[0][1]

    for event in events[1:]:
        event_start, event_end = event
        logger.debug(f'Processing event: start={event_start}, end={event_end}')
        if event_start > previous_event_end_time:
            free_times.append((previous_event_end_time.to(
                "local"), event_start.to("local")))

            logger.debug(f'Found free time: {free_times[-1]}')
        previous_event_end_time = max(previous_event_end_time, event_end)

    free_times = split_overnights(free_times)
    free_times = adjust_early_mornings_and_late_nights(
        free_times, start_after, end_before)
    # Return the list of identified free time slots for further use
    return free_times


def adjust_early_mornings_and_late_nights(slots, start_after, end_before):
    logger.debug('Adjusting time slots for early mornings and late nights')
    adjusted_slots = []
    
    for start, end in slots:
        logger.debug(f'Original slot: {start} to {end}')
        
        if start.time() < start_after:
            start = start.replace(hour=start_after.hour, minute=start_after.minute, second=start_after.second)
            logger.debug(f'Start time adjusted to meet start_after: {start}')
        
        if end.time() > end_before or end.time() == time(0, 0):
            end = end.replace(hour=end_before.hour, minute=end_before.minute, second=end_before.second)
            logger.debug(f'End time adjusted to meet end_before: {end}')
        
        adjusted_slots.append((start, end))
    
    logger.debug(f'Adjusted slots: {adjusted_slots}')
    return adjusted_slots


def split_overnights(slots):
    logger.debug('Splitting overnight time slots')
    split_slots = []

    for start, end in slots:
        logger.debug(f'Original slot: {start} to {end}')

        if start.date() != end.date():  # Check if the end time is on the next day
            # Create a split at midnight
            midnight = start.floor('day').shift(days=1)
            logger.debug(f'Splitting slot at midnight: {midnight}')

            # First slot: from start time to midnight
            first_slot = (start, midnight.shift(seconds=-1))
            split_slots.append(first_slot)
            logger.debug(f'First split slot: {first_slot}')

            # Second slot: from midnight to end time
            second_slot = (midnight, end)
            split_slots.append(second_slot)
            logger.debug(f'Second split slot: {second_slot}')

        else:
            # If not spanning midnight, just add the original slot
            split_slots.append((start, end))
            logger.debug(f'Slot does not span midnight, adding as is')

    logger.debug(f'Split slots: {split_slots}')
    return split_slots


def get_ics_events(ics_string, start_date, end_date):
    logger.debug('Parsing ICS string to extract events')
    
    if start_date.tzinfo is None:
        start_date = start_date.replace(tzinfo=arrow.now().tzinfo)
    if end_date.tzinfo is None:
        end_date = end_date.replace(tzinfo=arrow.now().tzinfo)
    calendar = Calendar(ics_string)
    events = []

    for event in calendar.events:
        rrule = None
        for extra in event.extra:
            if extra.name == 'RRULE':
                rrule = extra.value
        
        if not rrule:
            if start_date <= event.begin <= end_date or start_date <= event.end <= end_date :
                events.append((event.begin, event.end))
                logger.debug(f'Added event without recurrence: {event.name}, start: {event.begin}, end: {event.end}')
            continue

        dtstart_str = list(filter(lambda x: "DTSTART" in x, event.serialize().split("\r\n")))[0]
        try:
            rrule = rrulestr(f"{dtstart_str}\r\nRRULE:{rrule}")
            logger.debug(f'Parsed RRULE for event {event.name}: {rrule}')
        except ValueError as e:
            logger.error(f"Error parsing RRULE: {rrule} for event {event.name}: {e}")
            continue
        
        if rrule._tzinfo is None:
            occurrences = list(rrule.between(start_date.naive, end_date.naive, inc=True))
        else: 
            occurrences = list(rrule.between(start_date, end_date, inc=True))
        
        for occ in occurrences:
            occ_start = arrow.get(occ)
            occ_end = occ_start.shift(seconds=(event.end - event.begin).seconds)
            events.append((occ_start, occ_end))
            logger.debug(f'Added recurrent event: {event.name}, occurrence start: {occ_start}, occurrence end: {occ_end}')
    logger.info(f"Got {len(events)} events from ICS string")
    logger.debug(f'Extracted events: {events}')
    return events


def get_caldav_events(url, username, password, start_time, end_time):
    logger.info('Fetching events from CalDAV server')
    
    events_within_range = []
    
    # Connect to the CalDAV server
    with caldav.DAVClient(url, username=username, password=password) as client:
        # Get the list of calendars
        logger.debug('Fetching calendars')
        calendars = client.principal().calendars()
        
        # Convert start_time and end_time to UTC
        start_time_utc = start_time.replace(tzinfo=tz.tzlocal()).astimezone(tz.tzutc())
        end_time_utc = end_time.replace(tzinfo=tz.tzlocal()).astimezone(tz.tzutc())
        logger.debug(f'Searching events within range: {start_time_utc} - {end_time_utc}')

        for calendar in calendars:
            # Query events within the specified time range
            logger.debug(f'Searching events in calendar: {calendar.url}')
            events = calendar.search(event=True, start=start_time_utc, end=end_time_utc, expand=True)
            
            for event in events:
                # Extract the start and end times of the event
                event_start = event.instance.vevent.dtstart.value
                event_end = event.instance.vevent.dtend.value
                logger.debug(f'Found event: {event_start} - {event_end}')
                
                # Add the event to the result list
                events_within_range.append((arrow.get(event_start), arrow.get(event_end)))
                logger.debug(f'Added event to result list')

    logger.info(f'Fetched {len(events_within_range)} events from CalDAV server')
    return events_within_range

def filter_by_duration(slots, min_duration):
    logger.debug('Filtering time slots by duration')

    filtered_pairs = []

    for start_time, end_time in slots:
        logger.debug(f'Processing slot: {start_time} - {end_time}')

        if end_time - start_time >= min_duration:
            logger.debug('Slot meets minimum duration requirement, adding to filtered list')
            filtered_pairs.append((start_time, end_time))
        else:
            logger.debug('Slot does not meet minimum duration requirement, skipping')

    logger.debug(f'Filtered pairs: {filtered_pairs}')
    return filtered_pairs
