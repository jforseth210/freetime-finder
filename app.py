import sys
sys.path.append('.')
from dotenv import load_dotenv

from flask import Flask, render_template
from flask_migrate import Migrate
from flask_login import LoginManager, login_required, current_user
from blueprints.auth import auth
from blueprints.manage_calendars import manage_calendars
from blueprints.manage_events import manage_events
from db import db
import os
from models.User import User
basedir = os.path.abspath(os.path.dirname(__file__))

load_dotenv()

import logging

# Basic configuration for logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# Create a logger
logger = logging.getLogger(__name__)

def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = os.getenv("SECRET_KEY")  # Change this to a strong secret key
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'+os.path.join(basedir,'instance/database.db')
    app.config['INERTIA_TEMPLATE'] = 'inertia_base.html'

    db.init_app(app)
    migrate = Migrate(app, db) 

    # Initialize the login manager
    login_manager = LoginManager()
    login_manager.init_app(app)
    login_manager.login_view = "auth.login"

    app.register_blueprint(auth)
    app.register_blueprint(manage_calendars)
    app.register_blueprint(manage_events)

    @login_manager.user_loader
    def load_user(user_id):
        if user_id == "None":
            return None
        return User.query.filter_by(id=int(user_id)).first()

    @app.route('/')
    @login_required
    def index():
        users = User.query.all()
       

        return render_template("home.html", users=users, current_user=current_user)
    
    return app

if __name__ == '__main__':
    app = create_app()
    with app.app_context():
        db.create_all()
    app.run(debug=True, host="0.0.0.0")
