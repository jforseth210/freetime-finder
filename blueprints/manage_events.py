from flask import Blueprint, request, redirect, url_for, flash
from flask_login import current_user
from models.User import User
from models.Event import Event, TimeSlot
from db import db
import arrow
from brain import get_acceptee_events, find_free_times, filter_by_duration
from datetime import datetime,timedelta
manage_events = Blueprint('manage_events', __name__,
                        template_folder='templates')
@manage_events.route("/add-event", methods=["POST"])
def add_event():
    event_name = request.form.get('event_name')
    date_times = request.form.get('datetimes')
    hours = int(request.form.get('hours'))
    minutes = int(request.form.get('minutes'))
    
    start_after = arrow.get(request.form.get('startafter'), 'HH:mm')
    end_before = arrow.get(request.form.get('endbefore'), 'HH:mm')

    invitees = request.form.getlist('users[]')

    invitees = [User.query.filter_by(id=int(invitee)).first() for invitee in invitees]
    

    # Split the string into two parts using the ' - ' delimiter
    first_date, last_date = date_times.split(" - ")

    # Define the date format
    date_format = "%m/%d/%y %I:%M %p"

    # Parse the start and end times
    first_date = datetime.strptime(first_date, date_format)
    last_date = datetime.strptime(last_date, date_format)
    event = Event(current_user,event_name, hours, minutes, first_date, last_date, start_after, end_before, invitees) 
    db.session.add(event)
    db.session.commit()
    flash("Event added")
    return redirect("/")

@manage_events.route('/accept_event/<int:event_id>', methods=['POST'])
def accept_event(event_id):
    event = Event.query.filter_by(id=event_id).first()
    if event:
        current_user.invited_events.remove(event)
        current_user.accepted_events.append(event)
        db.session.commit()
    return redirect(url_for('index'))

@manage_events.route('/decline_event/<int:event_id>', methods=['POST'])
def decline_event(event_id):
    event = Event.query.filter_by(id=event_id).first()
    if event:
        current_user.invited_events.remove(event)
        current_user.declined_events.append(event)
        db.session.commit()
    return redirect(url_for('index'))

@manage_events.route('/solve-event', methods=['POST'])
def solve_event():
    event_id= request.form.get("event_id")
    event = Event.query.filter_by(id=event_id).first()
    acceptee_events = get_acceptee_events(event)
    free_times = find_free_times(acceptee_events, event.start_after, event.end_before)
    long_enough = filter_by_duration(free_times, timedelta(hours=event.hours, minutes=event.minutes))
    event.time_slots = []
    for slot in long_enough:
        start_time, end_time = slot
        slot = TimeSlot(start_time=start_time.datetime, end_time=end_time.datetime)
        event.time_slots.append(slot)
        db.session.add(slot)

    db.session.commit()
    return redirect("/")
@manage_events.route('/delete-event', methods=['POST'])
def delete_caldav():
    event_id = request.form.get('event_id')
    event = Event.query.filter_by(id=event_id).first()
    db.session.delete(event)
    db.session.commit()
    flash(f"Deleted event: {event.name}")
    return redirect("/")
