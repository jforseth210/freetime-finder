from flask import Blueprint, render_template, abort, request, redirect, url_for, flash
from jinja2 import TemplateNotFound
from flask_login import login_required, login_user, logout_user
from models.User import User
from db import db
from werkzeug.security import generate_password_hash, check_password_hash
auth = Blueprint('auth', __name__,
                        template_folder='templates')

@auth.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        user = User.query.filter_by(username=username).first()
        if user and check_password_hash(user.hashed_password, password):
            login_user(user)
            return redirect('/')
    return render_template("login.html")

@auth.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        captcha = request.form['captcha']
        if captcha.lower() not in ["gregory", "saint gregory", "st. gregory", "pope saint gregory", "gregory the great", "saint gregory the great", "st. gregory the great", "pope saint gregory the great"]:
            flash("Failed captcha")
            return "/signup"
        confirm_password = request.form['confirm-password']
        if (password == confirm_password):
            user = User(username, password)
            db.session.add(user)
            db.session.commit()
            login_user(user)
            return redirect("/?newuser")
    return render_template("signup.html")

@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.login'))

