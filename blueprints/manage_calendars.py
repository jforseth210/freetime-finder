from flask import Blueprint, request, redirect, flash
from flask_login import login_required, current_user
from models.Calendar import Webcal, CalFile, CaldavCredential
from db import db
import requests
from ics import Calendar
import caldav
manage_calendars = Blueprint('manage_calendars', __name__,
                        template_folder='templates')


@manage_calendars.route('/add-webcal', methods=["POST"])
@login_required
def add_webcal():
    url = request.form.get("calendar", "")
    url = url.replace("webcal://", "https://")
    if not url: 
        return redirect("/")
    if not current_user.is_authenticated:
        print("Unauthed user on this page!!!")
        return redirect("/login")

    try:
        response = requests.get(url)
        Calendar(response.text)
    except Exception as e:
        print(e)
        flash("Invalid link")
        return redirect("/")
    webcal = Webcal(current_user,url)
    db.session.add(webcal)
    db.session.commit()
    flash(f"Added {url}")
    return redirect("/")

@manage_calendars.route('/delete-webcal', methods=['POST'])
def delete_webcal():
    webcal_id = request.form.get('webcal_id')
    webcal = Webcal.query.filter_by(id=webcal_id).first()
    db.session.delete(webcal)
    db.session.commit()
    flash(f"Deleted calendar: {webcal.url}")
    return redirect("/")


@manage_calendars.route('/add-calfile', methods=["POST"])
@login_required
def add_calfile():
    # Check if the request has a file
    if 'calfile' not in request.files:
        return redirect("/")

    file = request.files["calfile"]


    if not current_user.is_authenticated:
        print("Unauthed user on this page!!!")
        return redirect("/login")

    # Read the file contents as a string
    contents = file.read()
    try:
        Calendar(contents.decode("utf-8"))
    except Exception:
        flash("Invalid calendar file")
        return redirect("/")
    
    # Create a new CalFile instance with user and file_contents
    calfile = CalFile(user=current_user,contents=contents)
    db.session.add(calfile)
    db.session.commit()
    flash(f"Added calendar file")
    return redirect("/")

@manage_calendars.route('/delete-calfile', methods=['POST'])
def delete_calfile():
    calfile_id = request.form.get('calfile_id')
    calfile = CalFile.query.filter_by(id=calfile_id).first()
    db.session.delete(calfile)
    db.session.commit()
    flash(f"Deleted calendar file: {calfile.get_name()}")
    return redirect("/")

@manage_calendars.route('/add-caldav', methods=["POST"])
@login_required
def add_caldav():
    url = request.form.get("url", "")
    username = request.form.get("username", "")
    password = request.form.get("password", "")



    if not current_user.is_authenticated:
        print("Unauthed user on this page!!!")
        return redirect("/login")
    try:
        with caldav.DAVClient(url, username=username, password=password) as client:
            # Get the list of calendars
            calendars = client.principal().calendars()
    except Exception:
        flash("Invalid caldav credentials")
        return redirect("/")

    dav_credentials = CaldavCredential(user=current_user,url=url, username=username, password=password)
    db.session.add(dav_credentials)
    db.session.commit()
    flash(f"Added caldav credentials for {url}")
    return redirect("/")
@manage_calendars.route('/delete-caldav-credential', methods=['POST'])
def delete_caldav():
    caldav_id = request.form.get('caldav_id')
    caldav = CaldavCredential.query.filter_by(id=caldav_id).first()
    db.session.delete(caldav)
    db.session.commit()
    flash(f"Deleted caldav credential: {caldav.url}")
    return redirect("/")
